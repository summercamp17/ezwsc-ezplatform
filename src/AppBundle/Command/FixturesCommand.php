<?php

namespace AppBundle\Command;

use DateTime;
use DateTimeZone;
use eZ\Publish\API\Repository\Values\ContentType\FieldDefinitionCreateStruct;
use eZ\Publish\Core\Base\Exceptions\NotFoundException;
use eZ\Publish\Core\Persistence\Legacy\Exception\TypeNotFound;
use eZ\Publish\Core\Repository\Values\ContentType\ContentTypeCreateStruct;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FixturesCommand extends ContainerAwareCommand
{
    public static $baseUrl = 'http://www.ezplatform.ez6/api/ezp/v2';
    public static $contentTypesUrl = '/content/typegroups/:ID/types';
    public static $contentTypeGroup = 1; // Matching 'Content' content type group

    public $sessionListFixture = ['DAY 0 – Tuesday, Aug 29', 'DAY 1 – Wednesday, Aug 30', 'DAY 2 – Thursday, Aug 31', 'DAY 3 – Friday, Sep 1', 'DAY 4 - Saturday, Sep 2' ];

    public $sessionsFixture = [
        '0' => [
            ['startDate' => 1504022400, 'endDate' => 1504027800, 'takeaways' => 'your laptop and some beers !', 'workshop_level' => 'intermediate', 'category' => 'global', 'author' => '', 'title' => 'Help Desk / Hanging Out ', 'description' => 'A special Help Desk for PHP, eZ, and JS tracks will be organized, with hanging out over complimentary welcome drinks. <br>Hanging out will be held in front of the conference halls (one floor below reception). Help Desk will be organized in one of the halls.']
        ],
        '1' => [
            ['startDate' => 1504071000, 'endDate' => 1504072800, 'takeaways' => 'your laptop and some beers !', 'workshop_level' => 'intermediate','category' => 'global', 'author' => '', 'title' => 'Morning recreation', 'description' => 'In a few words: easy-going, fun, and recreational. Go for a swim or a run early in the morning and start the day afresh.'],
            ['startDate' => 1504072800, 'endDate' => 1504077600, 'takeaways' => 'your laptop and some beers !', 'workshop_level' => 'intermediate','category' => 'global', 'author' => '', 'title' => 'Registration & Coffee Hangout', 'description' => 'In front of the conference halls (one floor below reception).'],
            ['startDate' => 1504077600, 'endDate' => 1504078200, 'takeaways' => 'your laptop and some beers !', 'workshop_level' => 'intermediate','category' => 'global', 'author' => '', 'title' => 'Shortest Opening Keynote Ever', 'description' => ''],
            ['startDate' => 1504116000, 'endDate' => 1504116000, 'takeaways' => 'your laptop and some beers !', 'workshop_level' => 'intermediate','category' => 'global', 'author' => '', 'title' => 'All Aboard Dinner', 'description' => 'Join us for dinner at the wonderful Oleander restaurant next to the Hotel Eden pool and the romantic promenade. Enjoy the lovely ambience of the restaurant\'s terrace in the pleasant company of other participants and companions.<br><br>Dinner is in the ticket price. Do not forget to wear your websc pass at all times.'],
            ['startDate' => 1504089900, 'endDate' => 1504093500, 'takeaways' => 'your laptop and some beers !', 'workshop_level' => 'intermediate','category' => 'global', 'author' => '', 'title' => 'Lunch', 'description' => 'Restaurant On, across the conference halls (one floor below reception).'],
            // EZ
            ['startDate' => 1504078200, 'endDate' => 1504089900, 'takeaways' => 'your laptop and some beers !', 'workshop_level' => 'advanced','category' => 'ez', 'author' => 'Petar Španja (Netgen)', 'title' => 'Real-life use cases leveraging Solr power', 'description' => ''],
            ['startDate' => 1504093500, 'endDate' => 1504105200, 'takeaways' => 'your laptop and some beers !', 'workshop_level' => 'intermediate','category' => 'ez', 'author' => 'Jérôme Vieilledent (Code Rhapsodie)', 'title' => 'EzCoreExtraBundle in practice (Intermediate)', 'description' => 'EzCoreExtraBundle provides several additional features to eZ Publish/eZ Platform, such as themes and other devx candies. In this workshop, you will experience different functionnalities that may help you in your projects.'],
            // UX
            ['startDate' => 1504116000, 'endDate' => 1504116000, 'takeaways' => 'your laptop and some beers !', 'workshop_level' => 'intermediate','category' => 'ux', 'author' => 'Stephanie Troeth', 'title' => 'Story techniques in design research – part 1', 'description' => 'Many of us have run some (or lots!) of usability testing sessions and interviews, and some of us may have done customer visits or contextual research. How can we take our research toolbox to the next level and make the most of the limited time we have with our users or customers? In this hands-on workshop, Steph will share methods on how to set up design research that has impact, longevity and depth, and frameworks to make sense of story-based data.'],
            ['startDate' => 1504093500, 'endDate' => 1504105200, 'takeaways' => 'your laptop and some beers !', 'workshop_level' => 'intermediate','category' => 'ux', 'author' => 'Stephanie Troeth', 'title' => 'Story techniques in design research – part 2', 'description' => 'Many of us have run some (or lots!) of usability testing sessions and interviews, and some of us may have done customer visits or contextual research. How can we take our research toolbox to the next level and make the most of the limited time we have with our users or customers? In this hands-on workshop, Steph will share methods on how to set up design research that has impact, longevity and depth, and frameworks to make sense of story-based data.'],
            // PHP
            ['startDate' => 1504116000, 'endDate' => 1504116000, 'takeaways' => 'your laptop and some beers !', 'workshop_level' => 'intermediate','category' => 'php', 'author' => 'Marco Perone & Stefano Maraspin', 'title' => 'Middleware architectures in PHP with Zend Expressive', 'description' => 'Middleware has recently become a must-know concept for PHP developers. With the advent of PSR-7, which standardized HTTP messages, many frameworks embraced and fostered the spread of such an approach making it easier for developers to create performant, composable, and customizable software architectures. In this workshop, you will incrementally develop a RESTful API using Zend Expressive, exploring its components and its workflow. You will discover how the framework allows to easily create simple and well organized code, exploiting open source libraries and middleware layers which could be reused for other projects, even with other frameworks!'],
            ['startDate' => 1504093500, 'endDate' => 1504105200, 'takeaways' => 'your laptop and some beers !', 'workshop_level' => 'intermediate','category' => 'php', 'author' => 'Paula Čučuk & Antonio Perić-Mažar', 'title' => 'Building APIs in an easy way using API Platform', 'description' => 'The web has changed! Users spend more time on mobile than on desktops and expect to have an amazing user experience on both. APIs are the heart of the new web as the central point of access data, encapsulating logic and providing the same data and same features for desktops and mobiles. In this workshop, Paula and Antonio will show you how to create complex APIs in an easy and quick way using API Platform built on Symfony.'],
            // JS
            ['startDate' => 1504116000, 'endDate' => 1504116000, 'takeaways' => 'your laptop and some beers !', 'workshop_level' => 'intermediate','category' => 'js', 'author' => 'Jason Lengstorf', 'title' => 'They came for the offline-first, but they stayed for the performance', 'description' => 'There\'s a big push for offline-first development, with a new buzzword — "Progressive Web App", or PWA — making the rounds for the last year or two. But a lot of what\'s being said sounds like the coding equivalent of "eat your vegetables!" What\'s in it for ME? Quite a bit, it turns out. In this talk, Jason will share the surprising ways online apps will see huge benefits when they\'re built to support offline users. Learn how to ace the Lighthouse test with just a few simple changes and how you can easily create offline-first apps with just a few lines of code; also how going offline-first will improve the connected experience, including better performance, increased stability and reliability, improved user experience, and more! So, in addition to eating your vegetables — you know, making your web apps usable for people with slow and unreliable connections or whatever — you\'ll ALSO see your apps get even better for connected users. And all with minimal development effort required.'],
            ['startDate' => 1504093500, 'endDate' => 1504105200, 'takeaways' => 'your laptop and some beers !', 'workshop_level' => 'basic','category' => 'js', 'author' => 'Luciano Mammino', 'title' => 'Universal JS web applications with React', 'description' => 'Since we started seeing JS on the server side, the developers\' dream has been to reduce the gap and the cost of switch between frontend and backend. Today with Node.js, React, and a whole ecosystem of tools, this dream is becoming true! In this workshop, Luciano is going to discuss Universal (a.k.a. Isomorphic) JS and present some practical examples regarding the major patterns related to routing, data retrieval, and rendering. He will use Node, React, webpack, Babel, and React Router, and give you a series of examples to get you started easily with this new technology trend.'],
        ],
        '2' => [
            ['startDate' => 1504157400, 'endDate' => 1504161000, 'takeaways' => 'your laptop and some beers !', 'workshop_level' => 'intermediate','category' => 'global', 'author' => '', 'title' => 'Morning recreation', 'description' => 'In a few words: easy-going, fun, and recreational. Go for a swim or a run early in the morning and start the day afresh.'],
            ['startDate' => 1504161000, 'endDate' => 1504164600, 'takeaways' => 'your laptop and some beers !', 'workshop_level' => 'intermediate','category' => 'global', 'author' => '', 'title' => 'Coffee Hangout', 'description' => 'In front of the conference halls (one floor below reception).'],
            ['startDate' => 1504191600, 'endDate' => 1504195200, 'takeaways' => 'your laptop and some beers !', 'workshop_level' => 'intermediate','category' => 'global', 'author' => '', 'title' => 'Web Battle', 'description' => 'Unconference session – a participant-driven meeting where anyone who wants to present a topic can claim a brief time to do so. A special award awaiting the highest-rated speaker!'],
            ['startDate' => 1504202400, 'endDate' => 1504202400, 'takeaways' => 'your laptop and some beers !', 'workshop_level' => 'intermediate','category' => 'global', 'author' => '', 'title' => 'All Aboard Dinner', 'description' => 'Join us for dinner at the wonderful Oleander restaurant next to the Hotel Eden pool and the romantic promenade. Enjoy the lovely ambience of the restaurant\'s terrace in the pleasant company of other participants and companions.<br><br>Dinner is in the ticket price. Do not forget to wear your websc pass at all times.'],
            // EZ
            ['startDate' => 1504164600, 'endDate' => 1504176300, 'takeaways' => 'your laptop and some beers !', 'workshop_level' => 'intermediate','category' => 'ez', 'author' => 'Edi Modrić (Netgen)', 'title' => 'Migrating legacy back-office modules in a future-proof way ', 'description' => ''],
            ['startDate' => 1504179900, 'endDate' => 1504191600, 'takeaways' => 'your laptop and some beers !', 'workshop_level' => 'intermediate','category' => 'ez', 'author' => 'Bertrand Dunogier (eZ Systems)', 'title' => 'Introduction to the Hybrid eZ Platform UI (Intermediate)', 'description' => 'With the introduction of a new admin interface for eZ Platform, a new world of customization is opening up for eZ Platform integrators. This workshop will present the architecture of the solution, and give an introduction to customizing and extending the UI for your projects and contributions.'],
            // UX
            ['startDate' => 1504164600, 'endDate' => 1504176300, 'takeaways' => 'your laptop and some beers !', 'workshop_level' => 'basic','category' => 'ux', 'author' => 'Chui Chui Tan', 'title' => 'Contextual experience design: Designing great user experience with context', 'description' => 'When we think of context, we often only think of location, or website versus mobile use. It\'s much more than that. Context could include the environment your users are in, their social circles, and even their tradition and custom. In this hands-on interactive workshop, you will explore ways to identify users\' needs, behaviours, and expectations based on their context, and design a great experience accordingly.'],
            ['startDate' => 1504179900, 'endDate' => 1504191600, 'takeaways' => 'your laptop and some beers !', 'workshop_level' => 'intermediate','category' => 'ux', 'author' => 'Stig Martin Fiskå', 'title' => 'Designing a customer journey from discovery to conversion', 'description' => 'A customer journey starts off-site. If you work with UX and customer experience, how should you think and design when planning interaction points all the way from the start of the customer journey until the end? Learn that in Stig\'s workshop'],
            // PHP
            ['startDate' => 1504164600, 'endDate' => 1504176300, 'takeaways' => 'your laptop and some beers !', 'workshop_level' => 'advanced','category' => 'php', 'author' => 'Marcello Duarte', 'title' => 'Lean and functional domain modelling', 'description' => 'Rich domain models, inspired by imperative objective-oriented approaches, dominate our industry. But lean domain models are a compelling alternative. By separating behaviour and state, lean models offer an effective way to represent units of behaviour. This session will explore how to represent a lean domain model using a functional programming paradigm. You\'ll explore how types can be used as a natural way to represent business constraints alongside smart constructors and functional validation. You will also learn how type properties can be modelled with property based testing, how behaviour can be better composed, how to keep domain behaviour isolated from application state, and much more.'],
            ['startDate' => 1504179900, 'endDate' => 1504191600, 'takeaways' => 'your laptop and some beers !', 'workshop_level' => 'intermediate','category' => 'php', 'author' => 'Marco Pivetta', 'title' => 'Basic CQRS and Event Sourcing with Prooph', 'description' => 'CQRS and Event Sourcing are challenging if approached for the first time, and especially if done from scratch. Marco will help you installing, configuring and getting Prooph to run. You\'ll build a fairly simple event-sourced aggregate in order for you to understand how to organize things inside CQRS/ES stack, and how to massively simplify some problems that usually cause very big performance issues when put at scale.'],
            // JS
            ['startDate' => 1504164600, 'endDate' => 1504176300, 'takeaways' => 'your laptop and some beers !', 'workshop_level' => 'intermediate','category' => 'js', 'author' => 'Marijn Haverbeke', 'title' => 'Working with JavaScript module systems ', 'description' => 'You\'ll be walking through the history of the way people have been managing dependencies in JavaScript, taking a look at how the various approaches work and why they were invented. The focus will be on "modern" module systems – CommonJS and ES6 modules – and how and when to use these without destroying your debugging experience or page-load speed.'],
            ['startDate' => 1504179900, 'endDate' => 1504191600, 'takeaways' => 'your laptop and some beers !', 'workshop_level' => 'intermediate','category' => 'js', 'author' => 'Željko Rumenjak & Domagoj Rukavina', 'title' => 'Supercharged React Native development with Shoutem', 'description' => 'React Native is an awesome technology for building native mobile apps. However, creating an app still remains a cumbersome task. Did we reach the maximum of development efficiency? Shoutem designed an architecture to eliminate all the boilerplate steps when building an app with its open-sourced plugins, cloud storage, and automated publishing of app to both stores. In this workshop, Tomislav will show you how to create and publish a unique production-ready application.'],

        ],
        '3' => [
            ['startDate' => 1504243800, 'endDate' => 1504247400, 'takeaways' => 'your laptop and some beers !', 'workshop_level' => 'intermediate','category' => 'global', 'author' => '', 'title' => 'Morning recreation', 'description' => 'In a few words: easy-going, fun, and recreational. Go for a swim or a run early in the morning and start the day afresh.'],
            ['startDate' => 1504247400, 'endDate' => 1504251000, 'takeaways' => 'your laptop and some beers !', 'workshop_level' => 'intermediate','category' => 'global', 'author' => '', 'title' => 'Coffee Hangout', 'description' => 'In front of the conference halls (one floor below reception).'],
            ['startDate' => 1504262700, 'endDate' => 1504266300, 'takeaways' => 'your laptop and some beers !', 'workshop_level' => 'intermediate','category' => 'global', 'author' => '', 'title' => 'Lunch', 'description' => 'Restaurant On, across the the conference halls (one floor below reception).'],
            ['startDate' => 1504278000, 'endDate' => 1504281600, 'takeaways' => 'your laptop and some beers !', 'workshop_level' => 'intermediate','category' => 'global', 'author' => 'Rachel Andrew', 'title' => 'Knowing It All, Closing keynote', 'description' => 'Rachel has been a web developer for over 20 years. Things have changed almost beyond recognition since she first sat down at a computer to figure out how to build a website. However some things haven’t changed, and perhaps in those things can be found a way to navigate all of the new things, to assess new technology, and to make the right decisions for the projects we work on.'],
            ['startDate' => 1504288800, 'endDate' => 1504288800, 'takeaways' => 'your laptop and some beers !', 'workshop_level' => 'intermediate','category' => 'global', 'author' => '', 'title' => 'Closing Dinner & Raffle', 'description' => 'Spend the last #websc evening dining in company of other participants and companions in a vibrant and colorful atmosphere of a Mexican restaurant La Concha. Mingle, sip Coronas or have a few shots of tequila, and enjoy tasty Mexican specialties. Also, everybody has a chance to participate in a raffle – you might just win.<br><br>Dinner is in the ticket price. Do not forget to wear your websc pass at all times.'],
            // EZ
            ['startDate' => 1504251000, 'endDate' => 1504262700, 'takeaways' => 'your laptop and some beers !', 'workshop_level' => 'basic','category' => 'ez', 'author' => 'David Buchmann (Liip AG) & Hrvoje Knežević (Netgen)', 'title' => 'Hands-on: HTTP caching with Varnish (Basic)', 'description' => 'For some scenarios, Varnish is the silver bullet to fix performance issues and go from a slow to a lightning fast site. However, more often than not, you will need to put some effort into your application to get good results. Ideally, you design your application to play well with caching from the outset. If you don\'t know what you are doing, Varnish can also be the bullet to shoot yourself in the foot. In this workshop, you will learn how to use HTTP headers to control caching and how to configure Varnish itself. Running your own Vagrant Box with Varnish and PHP, you get to try out the effects of the caching headers and Varnish configuration directives. You will discuss things that go well with Varnish and situations to avoid. After mastering the basics, you will look at advanced concepts like edge side includes (ESI).'],
            ['startDate' => 1504266300, 'endDate' => 1504278000, 'takeaways' => 'your laptop and some beers !', 'workshop_level' => 'intermediate','category' => 'ez', 'author' => 'Jani Tarvainen (eZ Systems) & Antonin Savoie (Kaliop)', 'title' => 'Extending eZ Platform REST API for building decoupled sites and applications (Intermediate)', 'description' => 'By using and extending the eZ Platform REST API, you can have a strong leverage to go multichannel. In this workshop, you will work on a full Single Page Application (SPA), using modern JavaScript components, which consumes the REST API and is decoupled from the backend completely.'],
            // UX
            ['startDate' => 1504251000, 'endDate' => 1504262700, 'takeaways' => 'your laptop and some beers !', 'workshop_level' => 'intermediate','category' => 'ux', 'author' => 'Rahel Anne Bailie', 'title' => 'Schemas, standards, and structure: Adaptive content as UX strategy ', 'description' => 'Content developers are facing an exponentially increasing challenge of creating enough content, in the right "shape" for the right audience. Right now, the shape is all about responsive design; the logical next step is to use adaptive content to shape the content experience in equal amounts. If we can agree that the end goal for audiences is to consume content – find content, get instructions, complete a transaction, be entertained, get informed – we can stretch the framework of UX to embrace content strategy. What goes into adaptive content is a combination of editorial and technical aspects of content. Using the right schema is on the technical side, alongside structurally-rich, semantically categorised content. Balancing that on the editorial side is structure that can handle multiple contexts. This workshop shows how these tensions are balanced with the underlying technologies and how it works together with UX to deliver a superior experience, especially when the need to deliver content – good, adaptive content – at scale is a consideration.'],
            ['startDate' => 1504266300, 'endDate' => 1504278000, 'takeaways' => 'your laptop and some beers !', 'workshop_level' => 'intermediate','category' => 'ux', 'author' => 'Kevin Cannon', 'title' => 'Interaction design beyond the pixels', 'description' => 'Interaction design is breaking away from the simple screen UIs in ever more exciting ways. We\'re being asked to put screens on all types of devices, everything has to be "smart" and our users are rarely just sitting at their desks while using our designs. Unfortunately, we don\'t yet have good tools for prototyping and designing these new experiences. In this workshop you\'ll learn how to design for these new products and contexts using hands-on, non-digital methods.'],
            // PHP
            ['startDate' => 1504251000, 'endDate' => 1504262700, 'takeaways' => 'your laptop and some beers !', 'workshop_level' => 'basic','category' => 'php', 'author' => 'Ryan Weaver', 'title' => 'Symfony Flex: The easier, faster, and brand new look of Symfony', 'description' => 'Symfony Flex is a new way of developing apps: faster, easier, and with the ability to start micro, and scale up to a huge app. Dive in and build something with Ryan!'],
            ['startDate' => 1504266300, 'endDate' => 1504278000, 'takeaways' => 'your laptop and some beers !', 'workshop_level' => 'advanced','category' => 'php', 'author' => 'Hannes Van De Vreken', 'title' => 'Add search to your PHP apps', 'description' => 'Get ready! This hands-on workshop will help you get introduced with Elasticsearch. Step by step, you\'ll improve an existing back-office application in Twitter Bootstrap to a search-powered application. Using existing PHP packages, this will be a breeze! No experience with Elasticsearch? No problem! All concepts will be explained in the first hour of the workshop.'],
            // JS
            ['startDate' => 1504251000, 'endDate' => 1504262700, 'takeaways' => 'your laptop and some beers !', 'workshop_level' => 'intermediate','category' => 'js', 'author' => 'Princiya Marina Sequeira', 'title' => 'Web of Things – Peer to Peer Web', 'description' => 'The web today is a growing universe. Over the years, web technologies have evolved to give web developers the ability to create new generations of useful web experiences. One such feature is WebRTC, which provides browsers and mobile applications with Real Time Communication (RTC) capabilities via simple JavaScript APIs. In this hands-on workshop you will learn to build applications to support real time communication on the web. You will build an app to get video and take snapshots with your webcam and share them peer-to-peer via WebRTC. Along the way, you\'ll learn how to use the core WebRTC APIs and set up a messaging server using Node.'],
            ['startDate' => 1504266300, 'endDate' => 1504278000, 'takeaways' => 'your laptop and some beers !', 'workshop_level' => 'basic','category' => 'js', 'author' => 'James Allardice', 'title' => 'Building a better login with the credential management API', 'description' => 'Login pages are probably the single type of page that users on the web interact with more than any other. In recent years the sign in experience has changed with the advent of federation via social networks, but whether a user has to type an email address and password or click a link and be redirected via Facebook, the process still interrupts the journey. The Credential Management API, a new W3C standard designed by Mike West at Google, is an attempt to help streamline this process at the user agent level. This workshop will investigate the new API and explore how we can use it to progressively enhance customer journeys in the apps we build.'],

        ],
        '4' => [
            ['startDate' => 1504339200, 'endDate' => 1504360800, 'takeaways' => 'your laptop and some beers !', 'workshop_level' => 'intermediate','category' => 'ez', 'author' => '', 'title' => 'Boat trip', 'description' => 'After 3 days of learning and improving your skills, you deserve a relaxing day at the boat trip with lunch on board!<br><br>Boat trip is complimentary. Do not forget to wear your websc pass at all times.']
        ],
    ];


    protected function configure()
    {
        $this->setName('restapi:generate-fixtures');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $repository = $this->getContainer()->get( 'ezpublish.api.repository' );
        $repository->setCurrentUser( $repository->getUserService()->loadUser( 14 ) );


        $output->writeln('0. Delete everything, just in case...');
        $this->clearEverything($output);

        $output->writeln('1. Creating content types');
        $this->createContentTypesByService($output);

        $output->writeln('2. Creating fixtures');
        $this->createContent($output);

        $output->writeln('3. Check that everything is here');
        $this->checkFixtures($output);
    }

    protected function clearEverything(OutputInterface $output)
    {
        $contentTypeService = $this->getContainer()->get('ezpublish.api.service.content_type');
        $contentService = $this->getContainer()->get('ezpublish.api.service.content');

        $output->writeln('<comment>Deleting sessions</comment>');
        foreach ($this->sessionsFixture as $dayIdentifier => $sessionsDay) {
            foreach($sessionsDay as $key => $contentInformations) {
                try {
                    $content = $contentService->loadContentByRemoteId('session_' . $dayIdentifier . '_' . $key);
                    $contentService->deleteContent($content->contentInfo);
                }
                catch (NotFoundException $e) {
                    // Do nothing
                }
                catch (\Exception $e) {
                    dump($e->getMessage());
                }
            }
        }

        $output->writeln('<comment>Deleting sessions_lists</comment>');
        foreach ($this->sessionListFixture as $key => $sessionListItem) {
            try {
                $content = $contentService->loadContentByRemoteId('session_list_' . $key);
                $contentService->deleteContent($content->contentInfo);
            } catch (NotFoundException $e) {
                    // Do nothing
            } catch (\Exception $e) {
                dump($e->getMessage());
            }
        }
        $output->writeln('<info>Done deleting contents</info>');

        $output->writeln('');

        $contentTypes = ['session_list', 'session'];
        $output->writeln('<comment>Deleting contentType </comment>');
        foreach ($contentTypes as $contentTypeIdentifier) {
            try {
                $contentType = $contentTypeService->loadContentTypeByIdentifier($contentTypeIdentifier);
                $contentTypeService->deleteContentType($contentType);
                $output->writeln('<info>Done deleting "' . $contentTypeIdentifier . '"</info>');
            } catch(\Exception $e) {
                dump($e->getMessage());
            }
        }

        $output->writeln('');
    }

    protected function checkFixtures(OutputInterface $output)
    {
        $contentService = $this->getContainer()->get('ezpublish.api.service.content');
        $missingFixtures = false;

        foreach ($this->sessionsFixture as $dayIdentifier => $sessionsDay) {
            foreach($sessionsDay as $key => $contentInformations) {
                try {
                    $contentService->loadContentByRemoteId('session_' . $dayIdentifier . '_' . $key);
                }
                catch (NotFoundException $e) {
                    $output->writeln('<error>Session : ' . $contentInformations['title'] . ' is missing</error>');
                    $missingFixtures = true;
                }
                catch (\Exception $e) {
                    dump($e->getMessage());
                }
            }
        }

        foreach ($this->sessionListFixture as $key => $sessionListItem) {
            try {
                $contentService->loadContentByRemoteId('session_list_' . $key);
            } catch (NotFoundException $e) {
                $output->writeln('<error>Session list : ' . $sessionListItem . ' is missing</error>');
                $missingFixtures = true;
            } catch (\Exception $e) {
                dump($e->getMessage());
            }
        }

        if ($missingFixtures) {
            $output->writeln('<error>Missing fixtures</error>');
        } else {
            $output->writeln('<info>Everything went well</info>');
        }
    }

    protected function createContentTypesByService(OutputInterface $output)
    {
        $contentTypeService = $this->getContainer()->get('ezpublish.api.service.content_type');
        $contentTypeGroup = $contentTypeService->loadContentTypeGroup('1');

        $output->writeln('<comment>Creating the content type "session"...</comment>');

        $contentTypeStruct = new ContentTypeCreateStruct(
            array(
                'identifier' => 'session',
                'mainLanguageCode' => 'eng-GB',
                'nameSchema' => '<title>',
                'names' => array( 'eng-GB' => 'Session' ),
                'fieldDefinitions' => $this->sessionFields()
            )
        );

        try {
            $contentTypeDraft = $contentTypeService->createContentType(
                $contentTypeStruct,
                array($contentTypeGroup)
            );

            $contentTypeService->publishContentTypeDraft($contentTypeDraft);
        } catch ( \Exception $e) {
            $output->writeln($e->getMessage());
            $output->writeln('<error>Fail to create the contentType</error>');

            return;
        }
        $output->writeln('<info>Done creating the content type "session"</info>');
        $output->writeln('');


        $output->writeln('<comment>Creating the content type "session_list"...</comment>');
        $contentTypeStruct = new ContentTypeCreateStruct(
            array(
                'identifier' => 'session_list',
                'mainLanguageCode' => 'eng-GB',
                'nameSchema' => '<title>',
                'names' => array( 'eng-GB' => 'Session list' ),
                'fieldDefinitions' => $this->sessionListFields(),
                'isContainer' => true
            )
        );

        try {
            $contentTypeDraft = $contentTypeService->createContentType(
                $contentTypeStruct,
                array($contentTypeGroup)
            );

            $contentTypeService->publishContentTypeDraft($contentTypeDraft);
        } catch ( \Exception $e) {
            $output->writeln($e->getMessage());
            $output->writeln('<error>Fail to create the contentType</error>');

            return;
        }
        $output->writeln('<info>Done creating the content type "session list"</info>');
        $output->writeln('');

        $output->writeln('<info>Success on  creating content types !</info>');
        $output->writeln('');
    }

    protected function createContent(OutputInterface $output)
    {
        $contentService = $this->getContainer()->get('ezpublish.api.service.content');
        $contentTypeService = $this->getContainer()->get('ezpublish.api.service.content_type');
        $locationService = $this->getContainer()->get('ezpublish.api.service.location');

        $contentTypeSessionList = $contentTypeService->loadContentTypeByIdentifier('session_list');
        $contentTypeSession = $contentTypeService->loadContentTypeByIdentifier('session');
        $location = $locationService->newLocationCreateStruct('2');


        $output->writeln('<comment>Starting to create ' . sizeof($this->sessionListFixture) . ' sessions list</comment>');
        $output->writeln('');

        foreach ($this->sessionListFixture as $dayIdentifier => $title) {
            $newContent = $contentService->newContentCreateStruct($contentTypeSessionList, 'eng-GB');
            $newContent->setField('title', $title);
            $newContent->remoteId = 'session_list_' . $dayIdentifier;

            try {
                $output->writeln('<comment>' . $title . '</comment>');
                $contentDraft = $contentService->createContent(
                    $newContent,
                    array($location)
                );

                $contentService->publishVersion($contentDraft->versionInfo);

            } catch(\Exception $e) {
                dump($e->getMessage());
                $output->writeln('<error>Failed to create content </error>');

                return;
            }
        }

        $output->writeln('<info>Done create sessions lists </info>');

        $output->writeln('');
        $output->writeln('<comment>Creating 22 sessions</comment>');
        $output->writeln('');


        foreach ($this->sessionsFixture as $dayIdentifier => $sessionsDay) {
            foreach($sessionsDay as $key => $contentInformations) {
                $timeZone = new DateTimeZone('UTC');

                $startDate = new DateTime('now', $timeZone);
                $startDate->setTimestamp($contentInformations['startDate']);


                $endDate = new DateTime('now', $timeZone);
                $endDate->setTimestamp($contentInformations['endDate']);

                $newContent = $contentService->newContentCreateStruct($contentTypeSession, 'eng-GB');
                $newContent->setField('title', $contentInformations['title']);
                $newContent->setField('description', $contentInformations['description']);
                $newContent->setField('takeaways', $contentInformations['takeaways']);
                $newContent->setField('workshop_level', $contentInformations['workshop_level']);
                $newContent->setField('startDate', $startDate);
                $newContent->setField('endDate', $endDate);
                $newContent->setField('protagonist', $contentInformations['author'] ? $contentInformations['author'] : '');
                $newContent->setField('category', $contentInformations['category']);
                $newContent->remoteId = 'session_' . $dayIdentifier . '_' . $key;

                $container = $contentService->loadContentByRemoteId('session_list_' . $dayIdentifier);
                $location = $locationService->newLocationCreateStruct($container->contentInfo->mainLocationId);

                try {
                    $output->writeln('<comment>' . $contentInformations['title'] . '</comment>');
                    $contentDraft = $contentService->createContent(
                        $newContent,
                        array($location)
                    );

                    $contentService->publishVersion($contentDraft->versionInfo);

                } catch(\Exception $e) {
                    dump($e->getMessage());
                    $output->writeln('<error>Failed to create content </error>');

                    return;
                }
            }
        }

        $output->writeln('');
    }

    protected function sessionFields()
    {
        $fields = [];

        $fields[] = new FieldDefinitionCreateStruct(
            array(
                'fieldTypeIdentifier' => 'ezstring',
                'identifier' => 'title',
                'names' => array( 'eng-GB' => 'Title' ),
                'position' => 10,
                'isRequired' => true,
                'isSearchable' => true,
            )
        );


        $fields[] = new FieldDefinitionCreateStruct(
            array(
                'fieldTypeIdentifier' => 'eztext',
                'identifier' => 'description',
                'names' => array( 'eng-GB' => 'Description' ),
                'position' => 20,
                'isRequired' => false,
                'isSearchable' => false,
            )
        );

        $fields[] = new FieldDefinitionCreateStruct(
            array(
                'fieldTypeIdentifier' => 'eztext',
                'identifier' => 'takeaways',
                'names' => array( 'eng-GB' => 'Takeaways' ),
                'position' => 22,
                'isRequired' => false,
                'isSearchable' => false,
            )
        );

        $fields[] = new FieldDefinitionCreateStruct(
            array(
                'fieldTypeIdentifier' => 'ezstring',
                'identifier' => 'workshop_level',
                'names' => array( 'eng-GB' => 'Workshop level' ),
                'position' => 24,
                'isRequired' => false,
                'isSearchable' => false,
            )
        );

        $fields[] = new FieldDefinitionCreateStruct(
            array(
                'fieldTypeIdentifier' => 'ezstring',
                'identifier' => 'protagonist',
                'names' => array( 'eng-GB' => 'Author' ),
                'position' => 25,
                'isRequired' => false,
                'isSearchable' => false,
            )
        );

        $fields[] = new FieldDefinitionCreateStruct(
            array(
                'fieldTypeIdentifier' => 'ezstring',
                'identifier' => 'category',
                'names' => array( 'eng-GB' => 'Category' ),
                'position' => 28,
                'isRequired' => true,
                'isSearchable' => true,
            )
        );

        $fields[] = new FieldDefinitionCreateStruct(
            array(
                'fieldTypeIdentifier' => 'ezdatetime',
                'identifier' => 'startDate',
                'names' => array( 'eng-GB' => 'Starting date' ),
                'position' => 30,
                'isRequired' => true,
                'isSearchable' => true,
            )
        );

        $fields[] = new FieldDefinitionCreateStruct(
            array(
                'fieldTypeIdentifier' => 'ezdatetime',
                'identifier' => 'endDate',
                'names' => array( 'eng-GB' => 'Endingdate' ),
                'position' => 40,
                'isRequired' => true,
                'isSearchable' => true,
            )
        );

        return $fields;
    }

    protected function sessionListFields()
    {
        $fields = [];

        $fields[] = new FieldDefinitionCreateStruct(
            array(
                'fieldTypeIdentifier' => 'ezstring',
                'identifier' => 'title',
                'names' => array( 'eng-GB' => 'Title' ),
                'position' => 10,
                'isRequired' => true,
                'isSearchable' => true,
            )
        );


        $fields[] = new FieldDefinitionCreateStruct(
            array(
                'fieldTypeIdentifier' => 'eztext',
                'identifier' => 'description',
                'names' => array( 'eng-GB' => 'Description' ),
                'position' => 20,
                'isRequired' => false,
                'isSearchable' => false,
            )
        );

        return $fields;
    }

    private function prepareContentTypes()
    {
        $data = array();

        $xml = new \SimpleXMLElement('<ContentTypeCreate/>');
        $xml->addChild('identifier', 'session');
        $names = $xml->addChild('names');
        $value = $names->addChild('value', 'Session');
        $value->addAttribute('languageCode', 'eng-GB');

        $descriptions = $xml->addChild('descriptions');
        $value = $descriptions->addChild('value', 'Description of a session');
        $value->addAttribute('languageCode', 'eng-GB');

        $xml->addChild('urlAliasSchema', '&lt;title&gt;');
        $xml->addChild('nameSchema', '&lt;title&gt;');
        $xml->addChild('mainLanguageCode', 'eng-GB');

        $fields = $xml->addChild('FieldDefinitions');

        $title = $fields->addChild('FieldDefinition');
        $title->addChild('identifier', 'title');
        $title->addChild('fieldType', 'ezstring');
        $title->addChild('fieldGroup', 'content');
        $title->addChild('position', '1');
        $title->addChild('isTranslatable', 'true');
        $title->addChild('isRequired', 'true');
        $title->addChild('defaultValue', 'my title');
        $title->addChild('isSearchable', 'true');
        $names = $title->addChild('names');
        $value = $names->addChild('value', 'Title');
        $value->addAttribute('languageCode', 'eng-GB');

        $descriptions = $title->addChild('descriptions');
        $value = $descriptions->addChild('value', 'This is the title');
        $value->addAttribute('languageCode', 'eng-GB');

        $startDate = $fields->addChild('FieldDefinition');
        $startDate->addChild('identifier', 'startDate');
        $startDate->addChild('fieldType', 'ezdate');
        $startDate->addChild('fieldGroup', 'content');
        $startDate->addChild('position', '2');
        $startDate->addChild('isTranslatable', 'false');
        $startDate->addChild('isRequired', 'true');
        $startDate->addChild('isSearchable', 'true');
        $names = $startDate->addChild('names');
        $value = $names->addChild('value', 'Starting date');
        $value->addAttribute('languageCode', 'eng-GB');

        $descriptions = $startDate->addChild('descriptions');
        $value = $descriptions->addChild('value', 'Starting date');
        $value->addAttribute('languageCode', 'eng-GB');

        $endDate = $fields->addChild('FieldDefinition');
        $endDate->addChild('identifier', 'endDate');
        $endDate->addChild('fieldType', 'ezdate');
        $endDate->addChild('fieldGroup', 'content');
        $endDate->addChild('position', '3');
        $endDate->addChild('isTranslatable', 'false');
        $endDate->addChild('isRequired', 'true');
        $endDate->addChild('isSearchable', 'true');
        $names = $endDate->addChild('names');
        $value = $names->addChild('value', 'Ending date');
        $value->addAttribute('languageCode', 'eng-GB');

        $descriptions = $endDate->addChild('descriptions');
        $value = $descriptions->addChild('value', 'Ending date');
        $value->addAttribute('languageCode', 'eng-GB');

        $description = $fields->addChild('FieldDefinition');
        $description->addChild('identifier', 'description');
        $description->addChild('fieldType', 'eztext');
        $description->addChild('fieldGroup', 'content');
        $description->addChild('position', '4');
        $description->addChild('isTranslatable', 'true');
        $description->addChild('isRequired', 'true');
        $description->addChild('defaultValue', 'my description');
        $description->addChild('isSearchable', 'true');
        $names = $description->addChild('names');
        $value = $names->addChild('value', 'Description');
        $value->addAttribute('languageCode', 'eng-GB');

        $descriptions = $description->addChild('descriptions');
        $value = $descriptions->addChild('value', 'This is the description');
        $value->addAttribute('languageCode', 'eng-GB');

        $data[] = $xml->asXML();

        $xml = new \SimpleXMLElement('<ContentTypeCreate/>');
        $xml->addChild('identifier', 'session_list');
        $names = $xml->addChild('names');
        $value = $names->addChild('value', 'List of sessions');
        $value->addAttribute('languageCode', 'eng-GB');

        $descriptions = $xml->addChild('descriptions');
        $value = $descriptions->addChild('value', 'Description of a session list');
        $value->addAttribute('languageCode', 'eng-GB');

        $xml->addChild('urlAliasSchema', '&lt;title&gt;');
        $xml->addChild('nameSchema', '&lt;title&gt;');
        $xml->addChild('mainLanguageCode', 'eng-GB');
        $xml->addChild('isContainer', 'true');

        $fields = $xml->addChild('FieldDefinitions');

        $title = $fields->addChild('FieldDefinition');
        $title->addChild('identifier', 'title');
        $title->addChild('fieldType', 'ezstring');
        $title->addChild('fieldGroup', 'content');
        $title->addChild('position', '1');
        $title->addChild('isTranslatable', 'true');
        $title->addChild('isRequired', 'true');
        $title->addChild('defaultValue', 'my title');
        $title->addChild('isSearchable', 'true');
        $names = $title->addChild('names');
        $value = $names->addChild('value', 'Title');
        $value->addAttribute('languageCode', 'eng-GB');

        $descriptions = $title->addChild('descriptions');
        $value = $descriptions->addChild('value', 'This is the title');
        $value->addAttribute('languageCode', 'eng-GB');

        $description = $fields->addChild('FieldDefinition');
        $description->addChild('identifier', 'category');
        $description->addChild('fieldType', 'ezstring');
        $description->addChild('fieldGroup', 'content');
        $description->addChild('position', '4');
        $description->addChild('isTranslatable', 'true');
        $description->addChild('isRequired', 'true');
        $description->addChild('defaultValue', 'my category');
        $description->addChild('isSearchable', 'true');
        $names = $description->addChild('names');
        $value = $names->addChild('value', 'Description');
        $value->addAttribute('languageCode', 'eng-GB');

        $descriptions = $description->addChild('descriptions');
        $value = $descriptions->addChild('value', 'This is the description');
        $value->addAttribute('languageCode', 'eng-GB');

        $data[] = $xml->asXML();

        return $data;
    }

    private function createContentTypes($output)
    {
        $contentTypes = $this->prepareContentTypes();

        foreach ($contentTypes as $contentType) {
            $curl = curl_init();
            $url = FixturesCommand::$baseUrl . str_replace(':ID', FixturesCommand::$contentTypeGroup, FixturesCommand::$contentTypesUrl);

            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_USERPWD, base64_encode("admin:publish"));
            curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

            curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Accept: application/vnd.ez.api.ContentType+xml',
                'Content-Type: application/vnd.ez.api.ContentTypeCreate+xml '
            ));
            curl_setopt($curl, CURLOPT_POSTFIELDS, $contentType);

            try {
                $response = curl_exec($curl);
                $info = curl_getinfo($curl);
                dump($info);
                dump($response);
                curl_close($curl);
            } catch (\Exception $e) {
                dump($e);
                curl_close($curl);
                die;
            }
        }
    }
}
