<?php

namespace AppBundle\Controller;

use AppBundle\Rest\values\Session;
use AppBundle\Rest\values\SessionList;
use DateTime;
use DateTimeZone;
use eZ\Publish\Core\Base\Exceptions\InvalidArgumentException;
use eZ\Publish\Core\REST\Server\Controller as BaseController;
use eZ\Publish\API\Repository\Values\Content\Query;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion\LogicalAnd;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion\ContentTypeIdentifier;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion\Field;
use eZ\Publish\API\Repository\Values\Content\Query\SortClause;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;


class APIController extends BaseController
{
    /**
     * @param $categoryName
     * @return SessionList
     *
     * @throws InvalidArgumentException
     */
    public function searchByCategoryAction($categoryName)
    {
        if (!in_array($categoryName, ['ez', 'js', 'ux', 'php', 'global'], true)) {
            throw new InvalidArgumentException('categoryName', 'Wrong category name');
        }

        $searchService = $this->container->get('ezpublish.api.repository')->getSearchService();

        $query = new Query(
            array(
                'filter' => new LogicalAnd(
                    array(
                        new ContentTypeIdentifier( 'session' ),
                        new Field( 'category', Criterion\Operator::EQ, $categoryName ),
                    )
                )
            )
        );

        try {
            $searchResult= $searchService->findContent($query);
            $items = [];

            foreach ($searchResult->searchHits as $item) {
                $items[] = new Session($item->valueObject);
            }

            return new SessionList($items);
        } catch (InvalidArgumentException $e) {
            throw $e;
        }
    }

    /**
     * @param $startDate
     * @param $endDate
     * @return SessionList
     *
     * @throws InvalidArgumentException
     */
    public function searchByDateAction($startDate, $endDate)
    {
        $searchService = $this->container->get('ezpublish.api.repository')->getSearchService();

        $timeZone = new DateTimeZone('Europe/Paris');
        $startDateTime = new DateTime('now', $timeZone);
        $startDateTime ->setTimestamp($startDate);

        $endDateTime = new DateTime('now', $timeZone);
        $endDateTime->setTimestamp($endDate);

        $query = new Query(
            array(
                'filter' => new LogicalAnd(
                    array(
                        new ContentTypeIdentifier( 'session' ),
                        new Field('startDate', Criterion\Operator::GTE, $startDateTime->format('U')),
                        new Field('endDate', Criterion\Operator::LT, $endDateTime->format('U'))
                    )
                )
            )
        );

        $query->sortClauses = array(new SortClause\Field('session', 'startDate', Query::SORT_ASC));

        try {
            $searchResult= $searchService->findContent($query);
            $items = [];

            foreach ($searchResult->searchHits as $item) {
                $items[] = new Session($item->valueObject);
            }

            return new SessionList($items);
        } catch (InvalidArgumentException $e) {
            throw $e;
        }
    }

}
