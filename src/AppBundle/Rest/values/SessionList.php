<?php

namespace AppBundle\Rest\values;

class SessionList
{
    public $sessions;

    public function __construct( $sessions )
    {
        $this->sessions = $sessions;
    }
}