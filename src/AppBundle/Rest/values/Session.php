<?php

namespace AppBundle\Rest\values;

use eZ\Publish\Core\Repository\Values\Content\Content;

class Session
{
    public $title;

    public $description;

    public $category;

    public $protagonist;

    public $startDate;

    public $endDate;

    public $_contentId;

    public $_locationId;

    public function __construct(Content $content)
    {
        $this->title = $content->getFieldValue('title')->text;
        $this->description = $content->getFieldValue('description')->text;
        $this->category = $content->getFieldValue('category')->text;
        $this->protagonist = $content->getFieldValue('protagonist')->text;
        $this->startDate = $content->getFieldValue('startDate')->value;
        $this->endDate = $content->getFieldValue('endDate')->value;

        $this->_contentId = $content->id;
        $this->_locationId = $content->contentInfo->mainLocationId;
    }
}