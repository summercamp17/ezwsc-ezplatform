<?php

namespace AppBundle\Rest\valueObjectVisitor;


use AppBundle\Rest\values\Session;
use eZ\Publish\Core\REST\Common\Output\ValueObjectVisitor;
use eZ\Publish\Core\REST\Common\Output\Generator;
use eZ\Publish\Core\REST\Common\Output\Visitor;

class SessionList extends ValueObjectVisitor
{
    public function visit( Visitor $visitor, Generator $generator, $data )
    {
        $generator->startObjectElement('SessionList');
        $generator->startList('Sessions');

        /** @var Session $session */
        foreach ($data->sessions as $session) {
            $generator->startObjectElement('Session');
            $generator->startValueElement('title', $session->title);
            $generator->endValueElement('title');

            $generator->startValueElement('description', $session->description);
            $generator->endValueElement('description');

            $generator->startValueElement('category', $session->category);
            $generator->endValueElement('category');

            $generator->startValueElement('protagonist', $session->protagonist);
            $generator->endValueElement('protagonist');

            $generator->startValueElement('startDate', $session->startDate->format('U'));
            $generator->endValueElement('startDate');

            $generator->startValueElement('endDate', $session->endDate->format('U'));
            $generator->endValueElement('endDate');

            $generator->startValueElement('_contentId', $session->_contentId);
            $generator->endValueElement('_contentId');

            $generator->startValueElement('_locationId', $session->_locationId);
            $generator->endValueElement('_locationId');

            $generator->endObjectElement('Session');
        }

        $generator->endList('Sessions');
        $generator->endObjectElement('SessionList');
    }
}