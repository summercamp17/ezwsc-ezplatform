# INSTALLATION

   - Clone this repository in /var/www/html/ezwsc-vuejs
   - Create a mysql user and set it in parameters.yml (default for websc : admin/null)
   - Run the command to create the database : `php app/console doctrine:database:create`
   - Run the command to create the eZ fresh install : `php app/console ezplatform:install clean`
   - Run the command to create needed data for the workshop : `php app/console restapi:generate-fixtures`


# VHOST

```
<VirtualHost *:80>
    ServerName websc-vuejs
    DocumentRoot /var/www/html/ezwsc-vuejs/web
    DirectoryIndex app.php

    # Set default timeout to 90s, and max upload to 48mb
    TimeOut 90
    LimitRequestBody 49152

    <Directory /var/www/ezwsc-ezplatform/web>
        Options FollowSymLinks
        AllowOverride None
        # Depending on your global Apache settings, you may need to comment this:
        Require all granted
    </Directory>

    # As we require ´mod_rewrite´  this is on purpose not placed in a <IfModule mod_rewrite.c> block
    RewriteEngine On

    # Environment.
    # Possible values: "prod" and "dev" out-of-the-box, other values possible with proper configuration
    # Defaults to "prod" if omitted. If Apache complains about this line and you can't install `mod_setenvif` then
    # comment out "%{ENV:SYMFONY_ENV}" line below, and comment this out or set via: SetEnv SYMFONY_ENV "prod"
    SetEnvIf Request_URI ".*" SYMFONY_ENV=prod

    # Sets the HTTP_AUTHORIZATION header sometimes removed by Apache
    RewriteRule .* - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization}]

    # Access to repository images in single server setup
    RewriteRule ^/var/([^/]+/)?storage/images(-versioned)?/.* - [L]

    # Makes it possible to place your favicon at the root `web` directory in your eZ instance.
    # It will then be served directly.
    RewriteRule ^/favicon\.ico - [L]
    RewriteRule ^/robots\.txt - [L]

    # The following rule is needed to correctly display assets from eZ / Symfony bundles
    RewriteRule ^/bundles/ - [L]

    # Additional Assetic rules for environments different from dev,
    # remember to run php app/console assetic:dump --env=prod
    RewriteCond %{ENV:SYMFONY_ENV} !^(dev)
    RewriteRule ^/(css|js|fonts?)/.*\.(css|js|otf|eot|ttf|svg|woff) - [L]

    RewriteRule .* /app.php
</VirtualHost>
```

# Hosts
Edit your hosts :
`sudoedit /etc/hosts`

And add the matching host :
`127.0.0.1 ezwsc-vuejs`

You can now go on http://websc-vues/ez and connect to the back office with default credentials : admin / publish


